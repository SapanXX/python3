# 1. Python truth values

- False and None evaluate to false
- Numeric zeros value: 0, 0.0, 0j
- Decimal(0), Fraction(0, x)
- Empty sequences/collections: '', (), [], {}
- Empty sets and range: set(). range(0)

&nbsp;

For custom objects they are bydefault considered to be true unless they override bool or len function return to 0

```
class myClass:
    def __bool__(self):
        return False

    def __len__(self):
        return 0
```

&nbsp;

# 2. Class String Functions

| **String Function** | **Called When** |
|---------------------|-----------------|
| object.\_\_str__(self)  | str(object), print(object), "{0}".format(object) |
| object.\_\_repr__(self) | repr(object) |
| object.\_\_format__(self, format_spec) | format(object, format_spec) |
| object.\_\_bytes__(self) | bytes(object) | 

&nbsp;

# 3. Class Attribute Functions

It is importannt to know that **getattr** is only called when requested attribute can't be find in the requested object, 
but the **getattribute** get called everytime.

| **String Function** | **Called When** |
|---------------------|-----------------|
| object.\_\_getattribute__(self)  | object.attr |
| object.\_\_getattr__(self) | object.attr |
| object.\_\_setattr__(self, attr, val) | object.attr = val |
| object.\_\_delattr__(self) | del object.attr |
| object.\_\_dir__(self) | dir(object) |

&nbsp;

# 4. Python Logging

- Usefull debugging feature
    - Not always preactical to debug in real time.
- Events can be categorized for easier analysis 
- Provide transaction record of a program's execution
- highly customized output

## Using the Logging Module
1. import logging
1. logging.debug("debug-level message") 
1. logging.info("info-level message")
1. logging.warning("warning-level message")
1. logging.error("error-level message")
1. logging.critical("critica-level message")

&nbsp;

| **Message Level** | **Description** |
|---------------------|-----------------|
| DEBUG | Diagniostic information useful for debugging |
| INFO  | General Information about program execution results |
| WARNING | Something unexpected, or an approaching problem |
| ERROR | Unable to perform a specific operation due to problem |
| CRITICAL | Program may not able to continue, serious error |

We can configure this using **logging.basicConfig(level = logging.DEBUG)** by setting the minimum level required. Default minimum level is WARNING.

**Note:**

By default the log output file open in append mode, so that it can grows continuely. Specifying "w" for writing mode overwrite the old log output file.

## Customizing logging
```
basicConfig(
    format = formatstr,
    datefmt = date_format_str
)
```

| **Format** | **Description** |
|------|---------|
| %(asctime)s | Human readable format when log was created |
| %(filename)s | Filename where the log message originated |
| %(funcName)s | Function name where log message originated |
| %(levelname)s | String representation of the message level (DEBUG, INFO, etc) |
| %(levelno)d | Numeric representation of the message level |
| %(lineno)d | Source line number where the logging call was issued (if available) |
| %(message)s | The logged message string itself |
| %(module)s | Module name portion of the filename where the message was logged |


&nbsp;

# 5. Class Comparision Operators

| **Comparision Function** | **Called When** |
|---------------------|-----------------|
| object.\_\_gt__(self, other)  | self > other |
| object.\_\_lt__(self, other)  | self < other |
| object.\_\_ge__(self, other)  | self >= other |
| object.\_\_le__(self, other)  | self <= other |
| object.\_\_eq__(self, other)  | self == other |
| object.\_\_ne__(self, other)  | self != other |

&nbsp;

# 6. Class Numeric Operators

| **Numeric Function** | **Called When** |
|---------------------|-----------------|
| object.\_\_add__(self, other)  | self + other |
| object.\_\_sub__(self, other)  | self - other |
| object.\_\_mul__(self, other)  | self * other |
| object.\_\_div__(self, other)  | self / other |
| object.\_\_floordiv__(self, other)  | self // other |
| object.\_\_pow__(self, other)  | self ** other |
| object.\_\_and__(self, other)  | self & other |
| object.\_\_or__(self, other)  | self \| other |
|---------------------|-----------------|
| object.\_\_iadd__(self, other)  | self += other |
| object.\_\_isub__(self, other)  | self -= other |
| object.\_\_imul__(self, other)  | self *= other |
| object.\_\_itruediv__(self, other)  | self /= other |
| object.\_\_ifloordiv__(self, other)  | self //= other |
| object.\_\_ipow__(self, other)  | self **= other |
| object.\_\_iand__(self, other)  | self &= other |
| object.\_\_ior__(self, other)  | self \|= other |