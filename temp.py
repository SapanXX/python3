import logging

extData = {
    'user' : 'sapanravidas@abc.com'    
}

def antherFunction():
    logging.debug("This is debug level mesage", extra=extData)

def main():
    formatstr = "User:%(user)s %(asctime)s : %(levelname)s : %(funcName)s Line:%(lineno)d %(message)s"
    datestr = "%m/%d/%Y %I:%M:%S %p"
    
    logging.basicConfig(level = logging.DEBUG, 
                        filename="tempout.log",
                        filemode="w",
                        format = formatstr,
                        datefmt=datestr
                        )
    
    
    
    logging.debug("debug-level message", extra=extData) 
    logging.info("info-level message", extra=extData)
    logging.warning("warning-level message", extra=extData)
    logging.error("error-level message", extra=extData)
    logging.critical("critica-level message", extra=extData)
    antherFunction()
    

if __name__ == '__main__':
    main()
     